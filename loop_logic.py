from fire_process import spread_fire
from anim import *
from buttons import *

#check_for_condition:
#used to determine if the animation should continue or not
#(condition in the while loop)
def check_for_condition(list_of_cells, fire_or_ash, nbr_of_cells):
    remain_fire_or_ash = False
    index = 0
    while((not remain_fire_or_ash) and (index < nbr_of_cells)):
        if(list_of_cells[index] == fire_or_ash):
            remain_fire_or_ash = True
        index += 1
    return remain_fire_or_ash

#spread_and_update:
#spreads the fire in list_of_cells and uses the function above to
#check if the condition allows the animation to start.
def spread_and_update(master, canvas, list_of_cells, rows, cols, list_of_squares, nbr_of_cells, rule):
    spread_fire(list_of_cells, rows, cols, rule)
    update_cell_status(master, canvas, list_of_cells, list_of_squares)
    remain_ashes = False
    remain_fire = False
    remain_ashes = check_for_condition(list_of_cells, "ash", nbr_of_cells)
    remain_fire = check_for_condition(list_of_cells, "fire", nbr_of_cells)

#recheck if the condition allows the animation to continue.
def is_fire_or_ashes_remaining(list_of_cells, nbr_of_cells):
    is_fire_or_ashes = True
    remain_ashes = False
    remain_fire = False
    remain_ashes = check_for_condition(list_of_cells, "ash", nbr_of_cells)
    remain_fire = check_for_condition(list_of_cells, "fire", nbr_of_cells)
    if(remain_fire == False and remain_ashes == False):
        is_fire_or_ashes = False
    return is_fire_or_ashes

#loop_logic:
#used when the start button is pressed with a left click.
def loop_logic(master, canvas, list_of_cells, rows, cols, list_of_squares, duration, rule, is_loop_running, button_start):
    #takes back all the options of the user while loop is running.
    disable_user_fire(canvas, is_loop_running, list_of_squares, button_start)
    nbr_of_cells = len(list_of_cells)
    #check for condition to start the loop.
    is_fire_or_ashes = is_fire_or_ashes_remaining(list_of_cells, nbr_of_cells)

    while(is_fire_or_ashes):
        #uses the user input to adjust the duration of the animation.
        master.after(int(duration * 1000), spread_and_update(master, canvas, list_of_cells, rows, cols, list_of_squares, nbr_of_cells, rule))
        master.update()
        #rechecks for condition.
        is_fire_or_ashes = is_fire_or_ashes_remaining(list_of_cells, nbr_of_cells)
    #gives back the options to the user.
    func_loop_logic = lambda master=master, canvas=canvas, list_of_cells=list_of_cells, rows=rows, cols=cols, list_of_squares=list_of_squares, duration=duration, rule=rule, is_loop_running=is_loop_running, button_start=button_start:loop_logic(master, canvas, list_of_cells, rows, cols, list_of_squares, duration, rule, is_loop_running, button_start)
    enable_user_fire(master, canvas, list_of_cells, rows, cols, list_of_squares, duration, is_loop_running, button_start, func_loop_logic)
