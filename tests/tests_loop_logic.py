import unittest
from loop_logic import *

class Testloop_logicFunctions(unittest.TestCase):

    def test_check_for_condition(self):
        list_of_cells_1 = ["void", "void", "void"]
        list_of_cells_2 = ["void", "fire", "void"]
        list_of_cells_3 = ["fire", "void", "ash"]

        self.assertEqual(check_for_condition(list_of_cells_1, "fire", 3), False)
        self.assertEqual(check_for_condition(list_of_cells_1, "ash", 3), False)
        self.assertEqual(check_for_condition(list_of_cells_2, "fire", 3), True)
        self.assertEqual(check_for_condition(list_of_cells_3, "ash", 3), True)
