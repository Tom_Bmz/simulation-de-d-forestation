import unittest
from init_list_of_cells import *

class Testinit_list_of_cellsFunctions(unittest.TestCase):

    def test_get_nbr_of_cells(self):
        self.assertEqual(get_nbr_of_cells(3, 4), 12)
        self.assertNotEqual(get_nbr_of_cells(3, 0), 12)

    def test_make_copy_of_list(self):
        original_list = ["tree", "tree", "void", "fire"]
        self.assertEqual(original_list, make_copy_of_list(original_list, 2, 2))
