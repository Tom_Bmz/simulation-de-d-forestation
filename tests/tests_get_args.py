import unittest
from get_args import *


class Testget_argsFunctions(unittest.TestCase):

    def test_get_data(self):
        self.assertEqual(get_default_value(10), 10)
        self.assertEqual(get_default_value(.8), .8)
        self.assertEqual(get_default_value("OK"), "OK")
