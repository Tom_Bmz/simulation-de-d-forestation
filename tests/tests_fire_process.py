import unittest
from fire_process import *


#Il reste à écrire le test de spread_fire qui reprend
#toutes les fonctions testées ici.
#Tous les tests fonctionnent.


class Testfire_processFunctions(unittest.TestCase):

    def test_spread_to_right(self):
        tab_to_modify = ["fire", "void"]
        res = ["fire", "void"]
        for index in range(len(tab_to_modify)):
            spread_to_right(tab_to_modify, index, 1)

        self.assertEqual(tab_to_modify, res)


    def test_spread_to_left(self):
        tab_to_modify = ["tree", "fire"]
        res = ["tree", "fire" ]
        for index in range(len(tab_to_modify)):
            spread_to_left(tab_to_modify, index, 1)

        self.assertEqual(tab_to_modify, res)


    def test_spread_above(self):
        tab_to_modify = ["tree", "fire"]
        res = ["fire", "fire"]
        for index in range(len(tab_to_modify)):
            spread_above(tab_to_modify, index, 1)

        self.assertEqual(tab_to_modify, res)


    def test_spread_below(self):
        tab_to_modify = ["fire", "fire", "tree", "tree"]
        res = ["fire", "fire", "fire", "fire"]
        for index in range(len(tab_to_modify)):
            spread_below(tab_to_modify, index, 2)

        self.assertEqual(tab_to_modify, res)


    def test_turn_to_ashes(self):
        tab_to_modify = ["fire", "tree", "void", "fire"]
        res = ["ash", "tree", "void", "ash"]

        turn_to_ashes(tab_to_modify)
        self.assertEqual(tab_to_modify, res)

    def test_turn_to_void(self):
        tab_to_modify = ["ash", "tree", "void", "ash"]
        res = ["void", "tree", "void", "void"]

        turn_to_void(tab_to_modify)
        self.assertEqual(tab_to_modify, res)

    def test_spread_all_directions(self):
        list_of_cells = ["void", "tree", "void", "tree", "fire", "tree", "void", "tree", "void"]
        res = ["void", "fire", "void", "fire", "fire", "fire", "void", "fire", "void"]

        spread_all_directions(list_of_cells, 4, 3)
        self.assertEqual(list_of_cells, res)



    def test_spread_fire(self):
        list_of_cells = ["ash", "tree", "void", "tree", "fire", "tree", "ash", "tree", "void"]
        res           = ["void", "fire", "void", "fire", "ash", "fire", "void", "fire", "void"]

        self.assertEqual(spread_fire(list_of_cells, 3, 3, 1), res)

    def test_total_neighboors_on_fire(self):
        list_of_cells = ["ash", "fire", "void", "fire", "tree", "fire", "ash", "fire", "void"]
        self.assertEqual(total_neighboors_on_fire(list_of_cells, 4, 3), 4)
