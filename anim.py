from tkinter import *

#update_cell_status :

#Changes the color of all of the cells in list_of_squares to the color of
#its equivalent in list_of_cells : Example :
#if list_of_cells[4] is set to "fire" then
#the color of list_of_squares[4] becomes "red" if isn't already.

def update_cell_status(master, canvas, list_of_cells, list_of_squares):
    for index in range(len(list_of_cells)):

        if(list_of_cells[index] == "fire"):
            if(list_of_squares[index] != "red"):
                canvas.itemconfig(list_of_squares[index], fill = "red")
                canvas.itemconfig(list_of_squares[index], activefill = "")

        if(list_of_cells[index] == "ash"):
            if(list_of_squares[index] != "grey"):
                canvas.itemconfig(list_of_squares[index], fill = "grey")


        if(list_of_cells[index] == "void"):
            if(list_of_squares[index] != "black"):
                canvas.itemconfig(list_of_squares[index], fill = "black")
