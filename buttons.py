from tkinter import *

#does_nothing :
#used to disable the start button when loop is running
def does_nothing():
    pass



#disable_user_fire :
#used when the loop that burns the trees is running
#takes back every options from the user while loop is running
def disable_user_fire(canvas, is_loop_running, list_of_squares, button_start):
    is_loop_running[0] = True
    #disables the start button with new command : "does_nothing"
    button_start.configure(text = "DISABLED", command=does_nothing)
    #disables the red color when hovering a tree
    for square in range(len(list_of_squares)):
        canvas.itemconfig(list_of_squares[square], activefill = "")


#enable_user_fire :
#used when the loop that burns the trees is not running
#gives back every options from the user while loop is not running
def enable_user_fire(master, canvas, list_of_cells, rows, cols, list_of_squares,
                    duration, is_loop_running, button_start, func_loop_logic):
    is_loop_running[0] = False
    #enables the start button with command : loop_logic (lambda)
    button_start.configure(text = "START", command=func_loop_logic)
    #enables the red color when hovering a tree
    for square in range(len(list_of_squares)):
        if((canvas.itemcget(list_of_squares[square], 'fill') == "lightgreen")):
            canvas.itemconfig(list_of_squares[square], activefill = "red")
