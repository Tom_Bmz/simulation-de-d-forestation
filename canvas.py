from tkinter import *
from fire_process import set_fire


#get_square_width :
#returns the width of one square only (later referred as "wdh")
def get_square_width(canvas_width, cols):
    return canvas_width / cols



#get_square_heigth :
#this function was previously used to make the squares fit in a predefined
#window, making the cells rectangles and not squares.
#returns the height of one square only (later referred as "hht")
def get_square_height(canvas_height, rows):
    return canvas_height / rows

#give_initial_color:
#returns the color (either "black" or "lightgreen") that will be given
#to the cell in list_of_cells[index]
def give_initial_color(list_of_cells, index):
    color = "black"
    if (list_of_cells[index] == "tree"):
        color = "lightgreen"
    return color

#turn_red:
#used when a left click occures on a cell:
#finds the square in list_of_squares that is hovered, turn it red and
#sets fire to its equivalent in list_of_cells
def turn_red(canvas, list_of_squares, list_of_cells, is_loop_running):
    if(is_loop_running[0] != True):
        index = 0
        while(canvas.itemcget(list_of_squares[index], 'tags') != "tree current"
        and index < len(list_of_squares)):
            index+= 1
        if(canvas.itemcget(list_of_squares[index], 'fill') == "lightgreen"):
            canvas.itemconfig(list_of_squares[index], fill = "red")
            canvas.itemconfig(list_of_squares[index], activefill = "")
            set_fire(list_of_cells, index)


#create_square:
#used to set the grid up
#creates one square on the canvas with the given coordinates.
#sets its color to "lightgreen" if it's a tree
#sets its color to "black" if it's void
def create_square(canvas, list_of_squares, x1, x2, y1, y2, index, color):
    if(color == "lightgreen"):
        square = canvas.create_rectangle(x1, y1, x2, y2, fill = color, activefill = "red", tags="tree")
    else :
        square = canvas.create_rectangle(x1, y1, x2, y2, fill = color)
    list_of_squares.append(square)

#loop_making_squares:
#uses the three functions above to create a square at the initial coordinates,
#then moves the coordinates
def loop_making_squares(canvas, wdh, hht, rows, cols, x1, x2, y1, y2, list_of_cells, list_of_squares, is_loop_running):
    index  = 0
    for row in range(rows):
        for col in range(cols):
            color = give_initial_color(list_of_cells, index)
            create_square(canvas, list_of_squares, x1, x2, y1, y2, index, color)
            #moving the width to create an entire row
            x1 += wdh
            x2 += wdh
            index += 1
        #moving the heigth to create the next row
        x1 = 2
        y1 += hht
        x2 = 2 + wdh
        y2 = y1 + hht

    #binds to all of the cells that contains a tree the function "turn_red"
    canvas.tag_bind("tree", "<Button-1>", lambda func:turn_red(canvas, list_of_squares, list_of_cells, is_loop_running))

#draw_squares:
#uses the functions above get_square_height, get_square_width and
#loop_making_squares to draw all of the squares.

def draw_squares(canvas, rows, cols, list_of_cells, list_of_squares, cell_size, is_loop_running):
    #gets dimensions.
    hht = get_square_height((cell_size * rows), rows)
    wdh = get_square_width((cell_size * cols), cols)
    #sets intial corrdinates up.
    x1 = 2
    y1 = 2
    x2 = x1 + wdh
    y2 = y1 + hht
    #starts loop
    loop_making_squares(canvas, wdh, hht, rows, cols, x1, x2, y1, y2, list_of_cells, list_of_squares, is_loop_running)
