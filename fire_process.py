from init_list_of_cells import make_copy_of_list
import random

#set_fire :
#sets list_of_cells[index] to "fire"
def set_fire(list_of_cells, index):
    list_of_cells[index] = "fire"

#kill_fire :
#sets list_of_cells[index] to "ash"
def kill_fire(list_of_cells, index):
    list_of_cells[index] = "ash"

#clean_ashes:
#sets list_of_cells[index] to "void"
def clean_ashes(list_of_cells, index):
    list_of_cells[index] = "void"

#Is_tree:
#finds out if list_of_cells[index] is set to "tree" or not.
#returns True or False
def Is_tree(list_of_cells, index):
    if(list_of_cells[index] == "tree"):
        return True
    else:
        return False

#Is_fire:
#finds out if list_of_cells[index] is set to "fire" or not.
#returns True or False
def Is_fire(list_of_cells, index):
    if(list_of_cells[index] == "fire"):
        return True
    else:
        return False

#Is_ash:
#finds out if list_of_cells[index] is set to "ash" or not.
#returns True or False
def Is_ash(list_of_cells, index):
    if(list_of_cells[index] == "ash"):
        return True
    else:
        return False

#turn_to_ashes:
#turn all of the cells in list_of_cells that are set to "fire" to "ashes"
def turn_to_ashes(list_of_cells):
    for index in range(len(list_of_cells)):
        if(Is_fire(list_of_cells, index)):
            kill_fire(list_of_cells, index)

#turn_to_void:
#turn all of the cells in list_of_cells that are set to "ash" to "void"
def turn_to_void(list_of_cells):
    for index in range(len(list_of_cells)):
        if(Is_ash(list_of_cells, index)):
            clean_ashes(list_of_cells, index)

#spread_to_right:
#sets to "fire" the cell in list_of_cells that is directly on the right of
#list_of_cells[index].
def spread_to_right(list_of_cells, index, cols):
    if(index < len(list_of_cells) - 1):
        if ((index + 1) % cols != 0):
            if(Is_tree(list_of_cells, index + 1)):
                set_fire(list_of_cells, index + 1)

#spread_to_left:
#sets to "fire" the cell in list_of_cells that is directly on the left of
#list_of_cells[index].
def spread_to_left(list_of_cells, index, cols):
    if(index > 0):
        if(index % cols != 0):
            if(Is_tree(list_of_cells, index - 1)):
                set_fire(list_of_cells, index - 1)

#spread_above:
#sets to "fire" the cell in list_of_cells that is directly above
#list_of_cells[index].
def spread_above(list_of_cells, index, cols):
    if(index >= cols):
        if(Is_tree(list_of_cells, index - cols)):
            set_fire(list_of_cells, index - cols)

#spread_below:
#sets to "fire" the cell in list_of_cells that is directly under
#list_of_cells[index].
def spread_below(list_of_cells, index, cols):
    if(index <= len(list_of_cells) - cols - 1):
        if(Is_tree(list_of_cells, index + cols)):
            set_fire(list_of_cells, index + cols)

#spread_in_all_direction :
#uses the four fonctions above the spread fire accordingly to Von Neumann.
def spread_all_directions(list_of_cells, index, cols):
    spread_to_right(list_of_cells, index, cols)
    spread_to_left(list_of_cells, index, cols)
    spread_above(list_of_cells, index, cols)
    spread_below(list_of_cells, index, cols)

#total_neighboors_on_fire:
#gives the number of cells set to "fire" next to the cell at a given index
def total_neighboors_on_fire(list_copy, index, cols):
    total_neighboors_on_fire = 0
    #below
    if(index <= len(list_copy) - cols - 1):
        if(Is_fire(list_copy, index + cols)):
            total_neighboors_on_fire += 1
    #above
    if(index >= cols):
        if(Is_fire(list_copy, index - cols)):
            total_neighboors_on_fire += 1
    #left
    if(index > 0):
        if(index % cols != 0):
            if(Is_fire(list_copy, index - 1)):
                total_neighboors_on_fire += 1
    #right
    if(index < len(list_copy) - 1):
        if ((index + 1) % cols != 0):
            if(Is_fire(list_copy, index + 1)):
                total_neighboors_on_fire += 1
    return total_neighboors_on_fire

#spread_with_randomness:
#used to spread fire accordingly to the second rule
def spread_with_randomness(list_of_cells, index, cols, list_copy):
    neighboors_on_fire = total_neighboors_on_fire(list_copy, index, cols)
    rand = random.random()
    if(list_of_cells[index] == "tree"):
        if(rand < 1 - (1 / (neighboors_on_fire + 1))):
            set_fire(list_of_cells, index)

#spread_fire:
#spreads fire on the entire list_of_cells
#(equivalent to one animation)
def spread_fire(list_of_cells, rows, cols, rule):
    #make a copy of list_of_cells to save the changes.
    list_copy = make_copy_of_list(list_of_cells, rows, cols)
    #ashes become void in list_of_cells (not the copy)
    turn_to_void(list_of_cells)
    #fires become ashes in list_of_cells (not the copy) (order is important)
    turn_to_ashes(list_of_cells)

    #spreads the fire for one animation
    if(rule == 1):
        for index in range(len(list_of_cells)):
            if(Is_fire(list_copy, index)):
                spread_all_directions(list_of_cells, index, cols)
    if(rule == 2):
         for index in range(len(list_of_cells)):
             if(Is_tree(list_of_cells, index)):
                 spread_with_randomness(list_of_cells, index, cols, list_copy)
    return list_of_cells
