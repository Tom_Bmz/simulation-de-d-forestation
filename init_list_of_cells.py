import random

#init_list_of_cells
#Initialize list_of_cells
def init_list_of_cells(nbr_of_cells, forestation_rate):

    list_of_cells = [condition(forestation_rate)]
    return list_of_cells

#get_nbr_of_cells:
#return the number of cells
def get_nbr_of_cells(rows, cols):
    return rows * cols


#condition:
#returns "tree" or "void" accordingly to the foresation rate.
def condition(forestation_rate):
    rand = random.random()
    if (rand < forestation_rate):
        return "tree"
    else:
        return "void"

#fill_list_with_random:
#set the entire list_of_cells to either "tree" or "void"
#accordingly to the foresation rate.
def fill_list_with_random(list_of_cells, nbr_of_cells, forestation_rate):
    for cell in range(0, nbr_of_cells -1):
        list_of_cells.append(condition(forestation_rate))


#get_finished_list:
#uses fonctions above to initialize a list, and fill it.
#returns to list.
def get_finished_list(nbr_of_cells, forestation_rate):
    list_of_cells = init_list_of_cells(nbr_of_cells, forestation_rate)
    fill_list_with_random(list_of_cells, nbr_of_cells, forestation_rate)
    return list_of_cells


#not used in code
#used for debugging purposes.
def display(list_of_cells, rows, cols):

    for row in range(rows):
        to_print = ""
        for col in range(cols):
            to_print += str(list_of_cells[cols * row + col])
            if(list_of_cells[cols * row + col] == "ash"):
                to_print += "   "
            else :
                to_print += "  "
        print(to_print)

#make_copy_of_list
#Makes a copy of a given list
def make_copy_of_list(list_of_cells, rows, cols):

    nbr_of_cells = get_nbr_of_cells(rows, cols)
    list_copy = []
    list_copy[0:] = list_of_cells[0:]
    return list_copy
