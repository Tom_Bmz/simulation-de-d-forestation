import argparse

#add_args:
#sets all of the args up for user input.
def add_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-rows', help = "number of rows between 1 and 30 (int)", type = int)
    parser.add_argument("-cols", help = "number of columns between 1 and 30 (int)", type = int)
    parser.add_argument("-duration", help = "number of secondes between each interations between 0.1 and 5 (float)", type = float)
    parser.add_argument("-cell_size", help = "size of one cell between 10 and 50 (int)", type = int)
    parser.add_argument("-afforestation", help = "probability of a cell being a tree between 0 and 1 (float)", type = float)
    parser.add_argument("-rule", help = "defines who the fire will spread, can only be set to 1 or 2", type = float)
    args = parser.parse_args()
    return args

#get_default_value:
#used solely to make things clearer.
def get_default_value(default_value):
    data = default_value
    return data

#get_rows:
#gets the user input on rows
#default value : 15
#number of rows must be between 1 and 30.
def get_rows(args):
    rows = get_default_value(15)
    if(args.rows is not None):
        if(args.rows < 1 or args.rows > 30):
            raise argparse.ArgumentTypeError("number of rows must be between 1 and 30")
        else:
            rows = args.rows
    return rows

#get_cols:
#gets the user input on columns
#default value : 25
#number of columns must be between 1 and 60.
def get_cols(args):
    cols = get_default_value(25)
    if(args.cols is not None):
        if(args.cols < 1 or args.cols > 60):
            raise argparse.ArgumentTypeError("number of columns must be between 1 and 60")
        else:
            cols = args.cols
    return cols

#get_cell_size:
#gets the user input on cell size
#default value : 35
#cell_size must be between 1 and 60.
def get_cell_size(args):
    cell_size = get_default_value(35)
    if(args.cell_size is not None):
        if(args.cell_size < 10 or args.cell_size > 50):
            raise argparse.ArgumentTypeError("cell_size must be between 10 and 50")
        else:
            cell_size = args.cell_size
    return cell_size

#get_afforestation:
#gets the user input on forestation rate
#default value : .6
#afforestation must be between 0 and 1.
def get_afforestation(args):
    afforestation = get_default_value(.6)
    if(args.afforestation is not None):
        args.afforestation = float(args.afforestation)
        if(args.afforestation < 0 or args.afforestation > 1):
            raise argparse.ArgumentTypeError("afforestation must be between 0 and 1")
        else :
            afforestation = args.afforestation
    return afforestation

#get_duration:
#gets the user input on duration of animation in secondes
#default value : .5 secondes
#afforestation must be between 0.1 and 5.0
def get_duration(args):
    duration = get_default_value(.5)
    if(args.duration is not None):
        args.duration = float(args.duration)
        if(args.duration < 0.1 or args.duration > 5.0):
            raise argparse.ArgumentTypeError("duration must be between 0.1 and 5.0")
        else :
            duration = args.duration
    return duration

#get_rule:
#gets the user input on the rule that will be used to spread fire
#default value : 1
#rule can only be set to 1 or 2
def get_rule(args):
    rule = get_default_value(1)
    if(args.rule is not None):
        args.rule = int(args.rule)
        if(args.rule != 1 and args.rule != 2):
            raise argparse.ArgumentTypeError("rule can only be set to 1 or 2")
        else :
            rule = args.rule
    return rule
