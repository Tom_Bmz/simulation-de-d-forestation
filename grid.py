from tkinter import *
from canvas import *
from loop_logic import loop_logic

#draw_grid:
#configures the grid of the window (like in css basically)
def draw_grid(master):
    master.rowconfigure(0, weight = 1)
    master.rowconfigure(1, weight = 1)
    master.columnconfigure(0, weight = 1)
    master.columnconfigure(1, weight = 1)

#set_up_grid:
#sets up the entire window, canvas with the cells ready, and the button with
#their commands
def set_up_grid(master, canvas, rows, cols, list_of_cells, list_of_squares, duration, rule, cell_size):

    #uses function above to draw the grid.
    draw_grid(master)

    is_loop_running = [False]
    #fills the canvas with squares.
    draw_squares(canvas, rows, cols, list_of_cells, list_of_squares, cell_size, is_loop_running)

    #initialize start button to start the animation.
    button_start = Button(master, text = "START", height = 2, width = 20)
    func_loop_logic = lambda master=master, canvas=canvas, list_of_cells=list_of_cells, rows=rows, cols=cols, list_of_squares=list_of_squares, duration=duration, rule=rule, is_loop_running=is_loop_running, button_start=button_start:loop_logic(master, canvas, list_of_cells, rows, cols, list_of_squares, duration,rule, is_loop_running, button_start)
    button_start.configure(command=func_loop_logic)
    button_start.grid(row = 1, column = 0)

    #initialize quit button.
    button_quit = Button(master, text = "QUIT", command = master.quit, height = 2, width = 20)
    button_quit.grid(row = 1, column = 1)
