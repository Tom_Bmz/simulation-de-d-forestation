Jules Goubelle, 
Tom Biomez, 
Groupe 5

Les arguments disponibles sont les suivants : 
    
    -rule : 1 ou 2. Valeur de défaut : 1
    Permet de définir la règle de propagation
    
    -cell_size : de 10 à 50. Valeur de défaut : 35
    Modifie la taille des carrés
    
    -afforestation : de 0 à 1. Valeur de défaut : 0.6
    Mofifie le taux de forestation
    
    -rows : de 1 à 30. Valeur de défaut : 15
    Modifie le nombre de lignes
    
    -cols : de 1 à 60. Valeur de défaut : 25
    Modifie le nombre de colonnes
    
    -duration : de 0.5 à 5.0 secondes. Valeur de défaut : 0.5
    Modifie la durée des animations
    
Exemple :
app.py rule=2 -cell_size=20 -rows=27 -cols=56 -afforestation = 0.9 -duration=0.1

    
    