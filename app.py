from init_list_of_cells import *
from get_args import *
from loop_logic import *
from canvas import *
from grid import *


def main():

    #affectations of all of the variables that need user input
    args = add_args()
    rows = get_rows(args)
    cols = get_cols(args)
    cell_size = get_cell_size(args)
    duration = get_duration(args)
    afforestation = get_afforestation(args)
    rule = get_rule(args)
    nbr_of_cells = get_nbr_of_cells(rows, cols)
    list_of_cells = get_finished_list(nbr_of_cells, afforestation)
    list_of_squares = []

    #sets up the window
    master = Tk()
    master.title("Simulation de feux de fôret")


    #sets the canvas up.
    canvas = Canvas(master, width = cell_size * cols + 2,
                    height = cell_size * rows + 5 )
    canvas.grid(row = 0, columnspan = 3)

    #sets the grid up according to user input
    set_up_grid(master, canvas, rows, cols, list_of_cells,
                list_of_squares, duration, rule, cell_size)

    master.mainloop()

if __name__ == "__main__":
    main()
